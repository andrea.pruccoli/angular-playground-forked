import { Observable } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';

/**
 * Creiamo un'interfaccia generica per tutti i componenti
 */
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

/**
 * Questa guard controlla che il componente implementi l'interfaccia
 * CanComponentDeactivate e la funzione canDeactivate; in tal caso
 * la richiama per valutarne il risultato, altrimenti torna true.
 */
@Injectable({
  providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate) {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
