import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar,
  ) { }

  /**
   * route e state vengono passati da Angular
   * nel momento in cui viene effettuata la navigazione tra le pagine.
   *
   * La possibilità di ritornare Observable|Promise|boolean, permette
   * l'operabilità in modo asincrono/sincrono.
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    // Observable
    return this.authService.isAuthenticated().pipe(
      map((authenticated: boolean) => {
        if (authenticated) {
          return true;
        } else {
          this.snackBar.open('Utente non autenticato. Impossibile accedere alla pagina.', null, {
            duration: 5000,
            verticalPosition: 'top',
          });
          console.log('Utente non autenticato, impossibile effettuare la navigazione.');
          // Per portarlo in uno state nostro
          // this.router.navigate(['/']);

          // Per restare nello state in cui si trova
          return false;
        }
      })
    );
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  /**
   * route viene passato da Angular nel momenti in cui si tenta
   * di navigare il ruote a cui canLoad fa riferimento
   */
  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(null, null);
  }
}
