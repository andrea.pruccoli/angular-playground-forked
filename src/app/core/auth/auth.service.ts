import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  loggedIn = false;

  /** Emuliamo una chiamata asincrona che controlla
   *  se effettivamente l'utente è autenticato al server
   */
  isAuthenticated() {
    // const promise = new Promise<boolean>(
    //   (resolve, reject) => {
    //     setTimeout(() => {
    //       resolve(this.loggedIn);
    //     }, 500);
    //   }
    // );

    // return promise;
    return of(this.loggedIn).pipe(delay(50));
  }

  login() {
    this.loggedIn = true;
    return of(this.loggedIn).pipe(delay(500));
  }

  logout() {
    this.loggedIn = false;
    return of(this.loggedIn).pipe(delay(500));
  }
}
