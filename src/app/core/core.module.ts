import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgxProgressModule } from "@kken94/ngx-progress";
import { FooterComponent } from "./layout/footer/footer.component";
import { HeaderComponent } from "./layout/header/header.component";

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    MatSnackBarModule,
    HttpClientModule,
    NgxProgressModule,
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    HeaderComponent,
    FooterComponent,
    NgxProgressModule,
  ],
})
export class CoreModule {}
