import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { ServiceComponent } from './service.component';

const route: Routes = [
  {
    path: '',
    component: ServiceComponent,
  }
];

@NgModule({
  declarations: [
    ServiceComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
  ]
})
export class ServiceModule { }
