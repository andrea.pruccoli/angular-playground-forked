import { Component, OnInit } from '@angular/core';

import { DataService } from 'src/app/services/data.service';
import { GitHubService } from 'src/app/services/github.service';
import { Dipendente } from 'src/app/features/models/dipendente';
import { GitHubCommit } from 'src/app/features/models/github';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  dipendenti: Dipendente[] = [];
  dipendentiAsync: Dipendente[] = [];
  commits: GitHubCommit[];

  constructor(
    private dataService: DataService,
    private githubService: GitHubService,
  ) { }

  ngOnInit() {
    this.dipendenti = this.dataService.getDipendenti();

    this.dataService.getDipendentiAsync()
      .subscribe(dipendenti => this.dipendentiAsync = dipendenti);

    this.githubService.getCommits('angular', 'angular')
      .subscribe(commits => this.commits = commits);
  }

}
