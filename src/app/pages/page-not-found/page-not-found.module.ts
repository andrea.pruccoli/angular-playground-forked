import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

import { PageNotFoundComponent } from './page-not-found.component';

const route: Routes = [
  {
    path: '',
    component: PageNotFoundComponent,
  }
];

@NgModule({
  declarations: [
    PageNotFoundComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
  ]
})
export class PageNotFoundModule { }
