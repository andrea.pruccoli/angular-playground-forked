import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

import { AmministrazioneComponent } from './amministrazione.component';

const route: Routes = [
  {
    path: '',
    component: AmministrazioneComponent,
  }
];

@NgModule({
  declarations: [
    AmministrazioneComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
  ]
})
export class AmministrazioneModule { }
