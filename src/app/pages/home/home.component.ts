import { Component, OnInit } from '@angular/core';

import { SupereroiService } from 'src/app/services/supereroi.service';
import { Supereroe } from 'src/app/features/models/supereroe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  supereroe: Supereroe;
  testo: String;

  constructor(private supereroiService: SupereroiService) { }

  ngOnInit() {
    this.supereroe = null;
    this.testo = 'Benvenuto al corso base su Angular';
  }

  onNuovoEroe() {
    // const supereroe: Supereroe = new Supereroe();
  }
}
