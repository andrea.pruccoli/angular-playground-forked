import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dipendente } from 'src/app/features/models/dipendente';

@Component({
  selector: 'app-resolve',
  templateUrl: './resolve.component.html',
  styleUrls: ['./resolve.component.scss']
})
export class ResolveComponent implements OnInit {
  dipendenti: Dipendente[] = [];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // Il parametro 'data' nella funzione è solo un wrapper, un placeholder
    // di tutti i dati ritornati dal resolver.
    // In questo caso aggiungiamo la dicitura : { dipendenti: Dipendente[] }
    // solo per specificare, all'interno di questa funzione, di che tipo
    // sia la variabile 'dipendenti' che andremo ad accedere, nel caso ci servisse
    // richiamare funzionalità relative a quel tipo
    this.route.data.subscribe((data: { dipendenti: Dipendente[]}) => {
      this.dipendenti = data.dipendenti;
    });

    // Questa è la dicitura senza specificare come è composto 'data'
    // this.route.data.subscribe((data) => {
    //   this.dipendenti = data.dipendenti;
    // });
  }

}
