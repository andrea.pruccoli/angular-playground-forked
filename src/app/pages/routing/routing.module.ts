import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

import { RoutingComponent } from './routing.component';
import { RoutingParametrizedComponent } from './routing-parametrized/routing-parametrized.component';

const route: Routes = [
  {
    path: '',
    component: RoutingComponent,
    children: [
      { path: ':id', component: RoutingParametrizedComponent }
    ],
  }
];

@NgModule({
  declarations: [
    RoutingComponent,
    RoutingParametrizedComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
  ]
})
export class RoutingModule { }
