import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-routing-parametrized',
  templateUrl: './routing-parametrized.component.html',
  styleUrls: ['./routing-parametrized.component.scss']
})
export class RoutingParametrizedComponent implements OnInit {
  paramIdSnapshot: any;
  paramIdSubscribed: any;
  paramIdObservable: Observable<any>;

  queryParamNameSnapshot: any;
  queryParamNameSubscribed: any;
  queryParamNameObservable: Observable<any>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // Recupero param id tramite snapshot
    this.paramIdSnapshot = this.route.snapshot.paramMap.get('id');

    // Recupero param id tramite subscribe all'observable
    // e valorizzazione della variabile
    this.route.paramMap.subscribe(
      (paramMap: ParamMap) => {
        this.paramIdSubscribed = paramMap.get('id');
      });

    // Recupero param id tramite Observable e async pipe nel template
    this.paramIdObservable = this.route.paramMap.pipe(
      // Utile per input di ricerca
      // switchMap((params: ParamMap) => {
      //   // SwitchMap ha bisogno di ritornare un Observable, in questo
      //   // caso lo fakeiamo
      //   return of(params.get('id'));
      // })
      map((paramMap: ParamMap) => paramMap.get('id')),
    );

    // Recupero query param name tramite snapshot
    this.queryParamNameSnapshot = this.route.snapshot.queryParamMap.get('name');

    // Recupero query param name tramite subscribe all'observable
    this.route.queryParamMap.subscribe(
      (paramMap: ParamMap) => {
        this.queryParamNameSubscribed = paramMap.get('name');
      });

    // Recupero query param name tramite Observable e async pipe
    this.queryParamNameObservable = this.route.queryParamMap.pipe(
      map((paramMap: ParamMap) => paramMap.get('name')),
    );
  }

}
