import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss']
})
export class RoutingComponent implements OnInit {
  selectedParamId: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  onLoadRouteAlternative(paramId: number, queryParamName?: string) {
    this.selectedParamId = paramId;

    this.router.navigate(['/router-parametrized', paramId], {
      queryParams: {
        name: queryParamName,
      },
    });
  }

  // Uso relativeTo
  onLoadRoute(paramId: number, queryParamName?: string) {
    this.selectedParamId = paramId;

    this.router.navigate([paramId], {
      relativeTo: this.route,
      queryParams: {
        name: queryParamName,
      },
    });
  }

}
