import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basic-forms',
  templateUrl: './basic-forms.component.html',
  styleUrls: ['./basic-forms.component.scss']
})
export class BasicFormsComponent implements OnInit {
  valore: string;

  name = new FormControl('');

  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl('', [Validators.required]),
  });

  constructor() { }

  ngOnInit() {
  }

  resetForm() {
    this.profileForm.setValue({
      firstName: '',
      lastName: '',
    });
  }

  setMyName() {
    this.profileForm.patchValue({
      firstName: 'Andrea',
    });
  }

  onSubmit() {
    console.log('Submitted!');
  }

}
