import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from 'src/app/shared/shared.module';

import { FormComponent } from './form.component';
import { BasicFormsComponent } from './basic-forms/basic-forms.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { DatiSupereroeModule } from 'src/app/features/components/dati-supereroe/dati-supereroe.module';
import { CanDeactivateGuard } from 'src/app/services/can-deactivate-guard.service';

const route: Routes = [
  {
    path: '',
    component: FormComponent,
    children: [
      // https://stackoverflow.com/questions/41716883/how-to-load-child-route-by-default-in-angular-2
      // NB: in questo caso è abbastanza safe perché in basic non abbiamo un altro router-outlet,
      // perché in quel caso saremmo dovuti stare moooooolto attenti
      {
        path: '',
        redirectTo: 'basic',
        pathMatch: 'full',
      },
      { path: 'basic', component: BasicFormsComponent },
      {
        path: 'template-driven',
        component: TemplateDrivenFormComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      { path: 'reactive', component: ReactiveFormComponent },
    ],
  },
];

@NgModule({
  declarations: [
    FormComponent,
    ReactiveFormComponent,
    TemplateDrivenFormComponent,
    BasicFormsComponent,
  ],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forChild(route),
    DatiSupereroeModule,
  ]
})
export class FormModule { }
