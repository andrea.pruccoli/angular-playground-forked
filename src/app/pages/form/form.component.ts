import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  subpage: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subpage = this.route.firstChild.routeConfig.path;
  }

  changePage(pageName: string) {
    this.subpage = pageName;
    this.router.navigate([pageName], { relativeTo: this.route });
  }
}
