import { Component, OnInit } from '@angular/core';
import { Supereroe, Identita } from 'src/app/features/models/supereroe';
import { CanComponentDeactivate } from 'src/app/services/can-deactivate-guard.service';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit, CanComponentDeactivate {

  poteri = ['Superforza', 'Volo', 'Invisibilità', 'Sensi di ragno'];

  model = new Supereroe(new Identita('Bruce', 'Banner'), this.poteri[0], 'Hulk');
  // model = new Supereroe(new Identita('Peter', 'Parker'), this.poteri[3], 'Spiderman');

  submitted = true;

  private edit = false;

  constructor() { }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.edit = false;
  }

  onEdit() {
    this.submitted = false;
    this.edit = true;
  }

  resetForm() {
    this.model = new Supereroe(new Identita(''), '');
  }

  canDeactivate() {
    if (!this.edit) {
      return true;
    }

    return confirm('Vuoi scartare le modifiche presenti?');
  }
}
