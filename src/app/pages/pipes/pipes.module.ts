import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { SharedPipesModule } from 'src/app/features/pipes/shared-pipes.module';

import { PipesComponent } from './pipes.component';

const route: Routes = [
  {
    path: '',
    component: PipesComponent,
  }
];

@NgModule({
  declarations: [
    PipesComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(route),
    SharedPipesModule,
  ]
})
export class PipesModule { }
