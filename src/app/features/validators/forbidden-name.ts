import { ValidatorFn, AbstractControl } from '@angular/forms';

/** Si possono definire nomi non validi con un'espressione regolare (regex) */
export function forbiddenNameValidator(nameRe: RegExp, toString?: (control: AbstractControl) => string): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const text = toString ? toString(control) : control.value;
    const forbidden = nameRe.test(text);
    return forbidden ? {forbiddenName: { value: text }} : null;
  };
}
