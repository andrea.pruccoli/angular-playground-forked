export class Dipendente {
  constructor(
    public id: number,
    public nome: string,
    public sede: string,
  ) {}
}
