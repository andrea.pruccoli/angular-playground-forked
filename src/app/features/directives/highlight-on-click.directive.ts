import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlightOnClick]',
})
export class HighlightOnClickDirective {

  // ElementRef è il riferimento all'elemento nel DOM host,
  // al quale la direttiva è applicata
  constructor(private el: ElementRef) { }

  // Chiamiamo la property come il nome della direttiva,
  // per poter usare direttamente quella nel template per
  // definire il colore
  @Input() appHighlightOnClick: string;

  // HostListener ci permette di catturare gli elementi del dom sull'elemento el
  @HostListener('click') onclick() {
    let backgroundColor = this.el.nativeElement.style.backgroundColor;

    this.appHighlightOnClick = this.appHighlightOnClick ? this.appHighlightOnClick : 'yellow';

    backgroundColor = backgroundColor ? null : this.appHighlightOnClick;

    this.el.nativeElement.style.backgroundColor = backgroundColor;
  }

}
