import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {
  private hasView = false;

  // TemplateRef ?
  // ViewContainerRef ?
  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef) { }

  // la keyword 'set' permette la rivalutazione ogni volta che
  // il valore a cui si è bindato cambia
  @Input() set appUnless(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }

}
